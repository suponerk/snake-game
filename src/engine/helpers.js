// Return random value between min:int and max:int
export function randomPos(min, max) {
  const val = Math.random() * (max - min) + min;
  return Math.floor(val);
}