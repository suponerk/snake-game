import { randomPos } from "./helpers.js";

export class Apple {
  constructor(canvas, gridStep) {
    this.canvas = canvas;
    this.context = this.canvas.getContext("2d");
    this.gridStep = gridStep;
    this.side = this.gridStep - 2;
    this.xPos = 0;
    this.yPos = 0;
    this.reset();
    //console.log("xPos", this.xPos);
    //console.log("yPos", this.yPos);
  }
  reset() {
    this.xPos = randomPos(0, this.canvas.width / this.gridStep);
    this.yPos = randomPos(0, this.canvas.height / this.gridStep);
  }
  draw(color = "lime") {
    this.context.fillStyle = color;
    this.context.fillRect(
      this.xPos * this.gridStep,
      this.yPos * this.gridStep,
      this.side,
      this.side
    );
  }
  getPos(){
    return {
      x: this.xPos,
      y: this.yPos
    }
  }
}
