import { randomPos } from "./helpers.js";

export class Snake {
  constructor(canvas, gridStep, length) {
    this.canvas = canvas;
    this.context = this.canvas.getContext("2d");
    this.minLen = 3;
    this.len = length ? length : this.minLen;
    this.gridStep = gridStep;
    this.side = this.gridStep - 2;
    this.body = [];
    this.velocity = {};
    this.reset();
  }

  reset() {
    this.body = [
      {
        x: randomPos(0, this.canvas.width / this.gridStep),
        y: randomPos(0, this.canvas.height / this.gridStep)
      }
    ];
    this.velocity = { x: 0, y: 0 };
  }

  draw(color = "blue") {
    this.context.fillStyle = color;
    for (let i = 0; i < this.body.length; i++) {
      this.context.fillRect(
        this.body[i].x * this.gridStep,
        this.body[i].y * this.gridStep,
        this.side,
        this.side
      );
    }
  }

  turn(direction) {
    // private
    // y is inverted
    if (direction === "down" && this.velocity.y !== -1) {
      this.velocity.x = 0;
      this.velocity.y = 1;
    }
    if (direction === "up" && this.velocity.y !== 1) {
      this.velocity.x = 0;
      this.velocity.y = -1;
    }
    if (direction === "left" && this.velocity.x !== 1) {
      this.velocity.x = -1;
      this.velocity.y = 0;
    }
    if (direction === "right" && this.velocity.x !== -1) {
      this.velocity.x = 1;
      this.velocity.y = 0;
    }
    //console.log("velocity: ", this.velocity.x, this.velocity.y);
  }

  move() {
    //debugger;
    //console.log("body", this.body);
    //console.log("velocity", this.velocity);
    let x = this.body[this.body.length - 1].x + this.velocity.x;
    let y = this.body[this.body.length - 1].y + this.velocity.y;
    this.body.push({ x, y }); //head of the snake
    // shrink snake length if actual length > minimal length
    if (this.body.length > this.len) this.body.shift();
  }

  grow() {
    this.len++;
  }

  die() {
    this.len = this.minLen;
    this.reset();
  }

  getHead() {
    const x = this.body[this.body.length - 1].x;
    const y = this.body[this.body.length - 1].y;
    return { x, y };
  }

  getTail() {
    const bodyLen = this.body.length;
    if (bodyLen <= 1) return null;
    //console.log(bodyLen)
    return this.body.slice(0, bodyLen - 1); //immutable
  }

  isIdle() {
    // idle means snake is not moving or it has no tail
    if (this.velocity.x === 0 && this.velocity.y === 0) return true;
    return false;
  }
}
