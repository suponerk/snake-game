import { Field } from "./Field.js";
import { Apple } from "./Apple.js";
import { Snake } from "./Snake.js";

window.onload = function() {
  const fps = 15;
  const gridStep = 15;
  const canvas = document.getElementById("field");

  const field = new Field(canvas);
  const apple = new Apple(canvas, gridStep);
  const snake = new Snake(canvas, gridStep);

  // Handle keyboard key press
  document.addEventListener("keydown", event => {
    handleKeyPush(event.keyCode);
  });

  //start the game
  setInterval(handleFrame, 1000 / fps);

  function handleKeyPush(keyCode) {
    if (keyCode === 37) snake.turn("left");
    if (keyCode === 38) snake.turn("up");
    if (keyCode === 39) snake.turn("right");
    if (keyCode === 40) snake.turn("down");
  }

  function handleFrame() {
    //debugger;
    field.draw();
    if (checkEat()) {
      snake.grow();
      apple.reset();
    }
    apple.draw();
    snake.move(); //next step
    //console.log("snake x: ", snake.getHead().x, ", y: ", snake.getHead().y);
    snake.draw();
    if (checkCollision(canvas, gridStep)) {
      console.log("collision");
      snake.draw("red");
      snake.die();
      apple.reset();
    }
  }

  function checkEat() {
    const head = snake.getHead();
    const applePos = apple.getPos();
    if (head.x === applePos.x && head.y === applePos.y) return true;
    return false;
  }

  function checkCollision(canvas, gridStep) {
    let idle = snake.isIdle();
    const tail = snake.getTail();
    //console.log(idle)
    if (!idle && tail) {
      //console.log("checking collision...");

      const head = snake.getHead();

      // check collision to walls
      const maxX = canvas.width / gridStep;
      const maxY = canvas.height / gridStep;
      if (head.x < 0 || head.x >= maxX) return true;
      if (head.y < 0 || head.y >= maxY) return true;

      //check collision to itself, 2 variants of realization:

      //let collision = false;
      //tail.map(item => {
      //  if (head.x === item.x && head.y === item.y) collision = true;
      //});
      //return collision;

      return tail
        .map(item => head.x === item.x && head.y === item.y)
        .reduce((sum, item) => sum || item);
    }
    return false;
  }
};
