export class Field {
  constructor(canvas) {
    this.canvas = canvas;
    this.context = this.canvas.getContext("2d");
    //console.log("canvas", this.canvas);
    //console.log("context", this.context);
  }
  draw(color = "black") {
    this.context.fillStyle = color;
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }
}
