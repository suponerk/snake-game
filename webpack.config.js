// to make absolute path
const path = require("path");

/*************************** Plugins ************************************/
// auto-integrate dynamically generated js files to html template
const HTMLWebpackPlugin = require("html-webpack-plugin");

// auto-delete old files (trash) from /dist folder
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
// copy favicon and any other files to prod without modification
const CopyWebpackPlugin = require("copy-webpack-plugin");
// CSS extract from html to separate file
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");
// JS uglifier and minifier
const TerserJSPlugin = require("terser-webpack-plugin");
// CSS minifier
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
/******************************************************************************/

console.log(">>> process.env.NODE_ENV", process.env.NODE_ENV);
const IS_DEVELOPMENT = process.env.NODE_ENV !== "production";
console.log(">>> devMode", IS_DEVELOPMENT);

const genFileName = extension => {
  return IS_DEVELOPMENT ? "[name]." + extension : "[name].[hash]." + extension;
};

const cssLoaders = preprocessor => {
  const loaders = [MiniCSSExtractPlugin.loader, "css-loader"];
  if (preprocessor) loaders.push(preprocessor);
  return loaders;
};

const fileLoaders = () => {
  const loaderName = genFileName("[ext]");
  const loaders = [{ loader: "file-loader", options: { name: loaderName } }];
  return loaders;
};

const cssProcessorPluginOptions = {
  // delete all comments from biuld
  preset: [
    "default",
    {
      discardComments: {
        removeAll: true
      }
    }
  ]
};

module.exports = {
  //default mode
  mode: "development",
  context: path.resolve(__dirname, "src"),
  entry: {
    // entry points
    main: "./index.js" // 1st entry point
  },
  output: {
    // output files
    // how we generate files. Filename generated dynamically
    filename: genFileName("js"),
    path: path.resolve(__dirname, "dist") // path where we will put all generated files
  },
  // array of plugins and their constructor() calls
  plugins: [
    new HTMLWebpackPlugin({
      template: "./index.html"
      //title: "My Webpack App" // title is ignored if already set it in template file
    }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "src/assets/favicon.ico"),
        to: path.resolve(__dirname, "dist")
      }
    ]),
    new MiniCSSExtractPlugin({ filename: genFileName("css") })
  ],
  module: {
    rules: [
      //{ test: /\.css$/, use: ["style-loader", "css-loader"] },
      { test: /\.css$/, use: cssLoaders() },
      { test: /\.(scss|sass)$/, use: cssLoaders("fast-sass-loader") },
      { test: /\.less$/, use: cssLoaders("less-loader") },
      { test: /\.(png|jpg|jpeg|svg|gif|pdf)$/, use: fileLoaders() },
      { test: /\.(ttf|woff|woff2|eof)$/, use: fileLoaders() },
      { test: /\.xml$/, use: ["xml-loader"] },
      { test: /\.csv$/, use: ["csv-loader"] }
    ]
  },
  devtool: IS_DEVELOPMENT ? "cheap-source-map" : false,
  resolve: {
    // rewrite defaults to disable ability to import other files without extensions
    extensions: [".js", ".jsx"]
  },
  optimization: {
    splitChunks: {
      // include all types of chunks
      chunks: "all"
    },
    minimize: IS_DEVELOPMENT ? false : true,
    minimizer: [
      new TerserJSPlugin({ extractComments: false }),
      new OptimizeCSSAssetsPlugin({ cssProcessorPluginOptions })
    ]
  }
};
