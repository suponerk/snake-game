# Snake game on pure JavaScript (ES6+) and WebPack.

Written on pure Vanilla JavaScript using Classes and modules.

Each class has its own individuality in state.

This game is written to learn ES6+ canvas and Webpack

Written in accordance with KISS principles.

### Quick Start
- Open "dist/index.html" in your web browser.
- JavaScript must be enabled.

### How to play
Just press any arrow button on keyboard, and snake begins to move ;)

You need to eat apples and prevent snake collisions.

Enjoy the feeling of KISS design

### How to build
- You need Node.js and NPM be installed in your system
- You need to unstall bash in you work under Windows
- Type `npm run build` in console (bash/sh) -or- `npm run dev` to build dev version
- Check your files in "dist" directory